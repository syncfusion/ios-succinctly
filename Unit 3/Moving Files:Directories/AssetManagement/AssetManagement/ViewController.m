//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    NSFileManager *sharedFM = [NSFileManager defaultManager];
    NSArray *paths = [sharedFM URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
    if ([paths count] > 0) {
        NSURL *libraryPath = paths[0];
        NSURL *sourcePath = [libraryPath   URLByAppendingPathComponent:@"someData.txt"];
        NSURL *destinationPath = [libraryPath
                                  URLByAppendingPathComponent:@"someOtherData.txt"];
        NSError *error = nil;
        BOOL success = [sharedFM moveItemAtURL:sourcePath
                                         toURL:destinationPath
                                         error:&error];
        if (success) {
            NSLog(@"Successfully moved %@ to %@",
                  sourcePath,
                  destinationPath);
        } else {
            NSLog(@"Could not move the file. Error: %@", error);
        }
}
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
