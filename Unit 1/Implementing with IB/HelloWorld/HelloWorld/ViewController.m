//
//  ViewController.m
//  HelloWorld
//
//  Created by Macbook Pro on 5/4/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize messageLabel = _messageLabel;
@synthesize nameField = _nameField;
@synthesize name=_name;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect]; [aButton setTitle:@"Say Hello" forState:UIControlStateNormal]; aButton.frame = CGRectMake(100.0, 200.0, 120.0, 40.0);
    [[self view] addSubview:aButton];
    // Configure an action.
    [aButton addTarget:self
                action:@selector(sayHello:)
      forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField { self.name = textField.text;
    if (textField == self.nameField) {
        [textField resignFirstResponder]; }
    return YES;
}

- (void)sayHello:(id)sender {
    
    if ([self.name length] == 0) {
        self.name = @"World";
    }
    _messageLabel.text = [NSString stringWithFormat:@"Hello, %@!", self.name];
    _messageLabel.textColor = [UIColor colorWithRed:0.0 green:0.3 blue:1.0
                                              alpha:1.0];
}

- (IBAction)sayGoodbye:(id)sender {
    if ([self.name length] == 0) {
        self.name = @"World";
    }
    _messageLabel.text = [NSString stringWithFormat:@"See you later, %@!", self.name];
    _messageLabel.textColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0
                                              alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
