//
//  ViewController.h
//  Audio
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController : UIViewController
{
       SystemSoundID	blipOne;
       SystemSoundID blip;
}

@property (readonly)	SystemSoundID	blip;

@end
